Aplicação que expõe uma api web que valide se uma senha é válida.

Foi adotado o Clean Architechture como design pattern para o desenvolvimento devido a sua capacidade de separar as regras de negocio das outras dependencias necessarias para a inicializacao da api.

O projeto foi separado em 3 camadas:
   *  Application -> Configuracao dos beans do projeto e properties
   *  Delivery -> Camada exposta da API, dtos e controlers...
   *  Domain -> Camada de servico, regras de negocio etc...

Obs: O Clean Architecture possui mais uma camada de integration, porem como esse projeto nao possui nenhuma chamada a servicos externos e a camada seria inutilizada a mesma foi desconsiderada.
   *  Integration -> Camada de integracao da API com banco de dados, chamadas Rest etc...

A validacao foi feita com a utilização de regex para facilitar o desenvolvimento e enxutar o codigo.

Foram criados os regex para respeitar as seguintes regras:
*  Nove ou mais caracteres 	-> (validado com String.length())		
*  Ao menos 1 dígito	-> "(.*[0-9].*)"
*  Ao menos 1 letra minúscula	-> "(.*[a-z].*)"
*  Ao menos 1 letra maiúscula	-> "(.*[A-Z].*)"
*  Ao menos 1 caractere especial	-> "(.*[!@#$%^&*()\\-+].*)"
   -  Considere como especial os seguintes caracteres: !@#$%^&*()-+
*  Não possuir caracteres repetidos dentro do conjunto 	-> ".*(.).*\\1+\\.*.*"
*  Não possuir espaços	-> "(.*\\s+.*)"

Alem disso foi separado cada regra por regex, tornando facil a inserção ou remoção de regras em uma alteração futura.
Foram criados os objetos PasswordDto.java e Password.java apenas para demonstrar a utilização dos parses ao longo dos modulos da aplicação, no caso de uma api produtiva poderia ter sido repassado apenas o password como string.

Alem disso a api conta tbm com rotas de healthCheck e metricas do Prometheus nas rotas abaixo:
http://localhost:8085/actuator/health
http://localhost:8085/actuator/prometheus

A documentação e testes da api, pode ser utilizado a rota do swagger abaixo:
http://localhost:8085/v2/api-docs
http://localhost:8085/swagger-ui.html

Passos concluidos de acordo com a ordem cronologica:

*  Criacao da casca do projeto (com spring boot)
*  Criacao de documentacao via swagger
*  Criacao dos actuators (health e prometheus)
*  Adicionar feature para teste integrado com cucumber
*  Desenvolvimento do teste integrado com cucumber
*  Desenvolvimento dos testes unitarios para a camada de delivery
*  Desenvolvimento do controller
*  Desenvolvimento dos testes unitarios para a camada de domain
*  Desenvolvimento das regras de negocio

Proximos passos:

*  Refatoracao do codigo

Feito por Luis Brienze. 
Linkedin: https://www.linkedin.com/in/luisbrienze/
Link do repo: https://gitlab.com/brienze1/password-api-iti