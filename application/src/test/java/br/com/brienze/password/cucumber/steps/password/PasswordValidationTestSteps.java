package br.com.brienze.password.cucumber.steps.password;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.brienze.password.Application;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import io.cucumber.datatable.DataTable;

@ContextConfiguration(classes = Application.class, loader = SpringBootContextLoader.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PasswordValidationTestSteps {

	@LocalServerPort
	private int serverPort;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private RestTemplate restTemplate;
	
	private String password;
	private List<String> passwordList;
	private ResponseEntity<Boolean> response;
	private Map<String, ResponseEntity<Boolean>> mapResponse;
	
	@PostConstruct
	public void init() {
		mapResponse = new HashMap<>();
	}
	
	@Dado("que foi informado o seguinte password {string}")
	public void que_foi_informado_o_seguinte_password(String password) {
		this.password = password;
	}

	@Dado("que foram informados os seguintes passwords para validacao")
	public void que_foram_informados_os_seguintes_passwords_para_validacao(DataTable dataTable) {
		passwordList = dataTable.asList();
	}

	@Dado("que foi informado um password nulo") 
	public void que_foi_informado_um_password_nulo() {
		password = null;
	}
	
	@Quando("for solicitada a validacao do password")
	public void for_solicitada_a_validacao_do_password() throws JsonProcessingException {
		Map<String, String> passwordMap = new HashMap<>();
		passwordMap.put("password", password);
		
		response = exchange("/v1/passwords/validate", mapper.writeValueAsString(passwordMap), Boolean.class);
	}

	@Quando("for solicitada a validacao dos passwords")
	public void for_solicitada_a_validacao_dos_passwords() throws JsonProcessingException {
		for (String password : passwordList) {
			Map<String, String> passwordMap = new HashMap<>();
			passwordMap.put("password", password);
			
			mapResponse.put(password, exchange("/v1/passwords/validate", mapper.writeValueAsString(passwordMap), Boolean.class));
		}
	}

	@Entao("deve ser retornado o boolean {string}")
	public void deve_ser_retornado_o_boolean(String bool) {
		Assert.assertEquals(String.valueOf(bool), response.getBody().toString());
	}

	@Entao("o status retornado deve ser {int}")
	public void o_status_retornado_deve_ser(Integer statusCode) {
		Assert.assertEquals(String.valueOf(statusCode), String.valueOf(response.getStatusCodeValue()));
	}
	
	@Entao("devem ser retornados os status abaixo para cada password")
	public void devem_ser_retornados_os_status_abaixo_para_cada_password(DataTable dataTable) {
		Map<String, String> expectedResponsesMap = dataTable.asMap(String.class, String.class);
		
		for (Entry<String, ResponseEntity<Boolean>> mappedResponse : mapResponse.entrySet()) {
			for (Entry<String, String> expectedResponse : expectedResponsesMap.entrySet()) {
				if(mappedResponse.getKey().equals(expectedResponse.getKey())) {
					Assert.assertEquals(expectedResponse.getValue().toString(), mappedResponse.getValue().getBody().toString());
				}
			}
		}
		
		
	}
	
	@Entao("o status retornado deve ser {int} para todas as respostas")
	public void o_status_retornado_deve_ser_para_todas_as_respostas(Integer statusCode) {
		for (ResponseEntity<Boolean> response : mapResponse.values()) {
			Assert.assertEquals(String.valueOf(statusCode), String.valueOf(response.getStatusCodeValue()));
		}
	}
	
	private <T> ResponseEntity<T> exchange(String path, Object request, Class<T> clazz){
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		
		HttpEntity<?> httpEntity = new HttpEntity<>(request, headers);
		
		ResponseEntity<T> response = restTemplate.exchange("http://localhost:" + serverPort + path, 
				HttpMethod.POST, 
				httpEntity, 
				clazz);
		
		return response;
	}
	
}
