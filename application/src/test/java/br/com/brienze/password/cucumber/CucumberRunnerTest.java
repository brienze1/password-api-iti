package br.com.brienze.password.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features", 
		tags = { "@PasswordValidationTest" },
		plugin = "pretty")
public class CucumberRunnerTest {

}
