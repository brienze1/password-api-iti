# language: pt
@PasswordValidationTest
Funcionalidade: Teste de validade password 

	O sistema deve realizar a validacao do password de forma correta seguindo as seguintes restricoes:
	1-) Nove ou mais caracteres
	2-) Ao menos 1 dígito
	3-) Ao menos 1 letra minúscula
	4-) Ao menos 1 letra maiúscula
	5-) Ao menos 1 caractere especial
	6-) Considere como especial os seguintes caracteres: !@#$%^&*()-+
	7-) Não possuir caracteres repetidos dentro do conjunto
	9-) Espaços em branco não devem ser considerados como caracteres válidos.
	10-) quanto for solicitada a validacao e a senha respeitar as regras acima entao deve ser retornado um boolean true
	11-) quanto for solicitada a validacao e a senha nao respeitar as regras acima entao deve ser retornado um boolean false
	
	Cenario: Password validado com sucesso
		Dado que foi informado o seguinte password "AbTp9!fok"
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "true"
		E o status retornado deve ser 200
	
	Cenario: Password validado com sucesso caracteres especiais
		Dado que foram informados os seguintes passwords para validacao 
		|	AbTp9!fok	|
		|	AbTp9@fok	|
		|	AbTp9#fok	|
		|	AbTp9$fok	|
		|	AbTp9%fok	|
		|	AbTp9^fok	|
		|	AbTp9&fok	|
		|	AbTp9*fok	|
		|	AbTp9(fok	|
		|	AbTp9)fok	|
		|	AbTp9-fok	|
		|	AbTp9+fok	|
		Quando for solicitada a validacao dos passwords
		Entao devem ser retornados os status abaixo para cada password
		|	AbTp9!fok	|	true	|
		|	AbTp9@fok	|	true	|
		|	AbTp9#fok	|	true	|
		|	AbTp9$fok	|	true	|
		|	AbTp9%fok	|	true	|
		|	AbTp9^fok	|	true	|
		|	AbTp9&fok	|	true	|
		|	AbTp9*fok	|	true	|
		|	AbTp9(fok	|	true	|
		|	AbTp9)fok	|	true	|
		|	AbTp9-fok	|	true	|
		|	AbTp9+fok	|	true	|
		E o status retornado deve ser 200 para todas as respostas
	
	Cenario: Validacoes utilizadas pelo exemplo
		Dado que foram informados os seguintes passwords para validacao 
		|	aa			|
		|	ab			|
		|	AAAbbbCc	|
		|	AbTp9!foo	|
		|	AbTp9!foA	|
		|	AbTp9 fok	|
		|	AbTp9!fok	|
		Quando for solicitada a validacao dos passwords
		Entao devem ser retornados os status abaixo para cada password
		|	aa			|	false	|
		|	ab			|	false	|
		|	AAAbbbCc	|	false	|
		|	AbTp9!foo	|	false	|
		|	AbTp9!foA	|	false	|
		|	AbTp9 fok	|	false	|
		|	AbTp9!fok	|	true	|
		E o status retornado deve ser 200 para todas as respostas
	
	Cenario: Password invalidado com erro de password em branco
		Dado que foi informado o seguinte password " "
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "false"
		E o status retornado deve ser 200
	
	Cenario: Password invalidado com erro de password nulo
		Dado que foi informado um password nulo
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "false"
		E o status retornado deve ser 200
	
	Cenario: Password invalidado com erro de quantidade de caracteres menor que nove
		Dado que foi informado o seguinte password "AbTp9!fo"
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "false"
		E o status retornado deve ser 200
	
	Cenario: Password invalidado com erro de nao contem digitos
		Dado que foi informado o seguinte password "AbTpz!fok"
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "false"
		E o status retornado deve ser 200
	
	Cenario: Password invalidado com erro de sem letras minusculas
		Dado que foi informado o seguinte password "ATTP9!FOK"
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "false"
		E o status retornado deve ser 200
	
	Cenario: Password invalidado com erro de sem letras maiusculas
		Dado que foi informado o seguinte password "abtp9!fok"
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "false"
		E o status retornado deve ser 200
	
	Cenario: Password invalidado com erro de sem caracteres especiais
		Dado que foi informado o seguinte password "abtp9ifok"
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "false"
		E o status retornado deve ser 200
	
	Cenario: Password invalidado com erro de caracteres repetidos
		Dado que foi informado o seguinte password "AboTp9!fok"
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "false"
		E o status retornado deve ser 200
	
	Cenario: Password invalidado com erro de contem espaco
		Dado que foi informado o seguinte password "AbTp 9!fok"
		Quando for solicitada a validacao do password
		Entao deve ser retornado o boolean "false"
		E o status retornado deve ser 200
		
