package br.com.brienze.password.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class BeanConfig {
	
	@Bean
	@Profile("test")
	public RestTemplate restTemplate() {
		 return new RestTemplate();
	}
	
	@Bean
	@Profile("test")
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
	
}
