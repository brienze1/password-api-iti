package br.com.brienze.password.endpoint;

import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.brienze.password.dto.PasswordDto;
import br.com.brienze.password.entity.Password;
import br.com.brienze.password.parse.PasswordParse;
import br.com.brienze.password.service.PasswordService;

@ExtendWith(SpringExtension.class)
public class PasswordControllerTest {

	@InjectMocks
	private PasswordController passwordController;
	
	@Mock
	private PasswordParse passwordParse;
	
	@Mock
	private PasswordService passwordService;
	
	@Test
	public void validateTest() {
		PasswordDto passwordDto = new PasswordDto();
		passwordDto.setPassword(UUID.randomUUID().toString());

		Password password = new Password();
		password.setPassword(passwordDto.getPassword());
		
		Password passwordResponse = new Password();
		passwordResponse.setPassword(passwordDto.getPassword());
		passwordResponse.setValid(true);

		PasswordDto passwordDtoResponse = new PasswordDto();
		passwordDtoResponse.setPassword(passwordDto.getPassword());
		passwordDtoResponse.setValid(true);
		
		Mockito.when(passwordParse.toPassword(passwordDto)).thenReturn(password);
		Mockito.when(passwordService.validate(password)).thenReturn(passwordResponse);
		Mockito.when(passwordParse.toPasswordDto(passwordResponse)).thenReturn(passwordDtoResponse);
		
		ResponseEntity<Boolean> response = passwordController.validate(passwordDto);
		
		Assertions.assertTrue(response.getBody());
	}
	
}
