package br.com.brienze.password.parse;

import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.brienze.password.dto.PasswordDto;
import br.com.brienze.password.entity.Password;

@ExtendWith(SpringExtension.class)
public class PasswordParseTest {

	@InjectMocks
	private PasswordParse passwordParse;
	
	@Test
	public void toPasswordTest() {
		PasswordDto passwordDto = new PasswordDto();
		passwordDto.setPassword(UUID.randomUUID().toString());
		
		Password password = passwordParse.toPassword(passwordDto);
		
		Assertions.assertEquals(passwordDto.getPassword(), password.getPassword());
		Assertions.assertEquals(passwordDto.isValid(), password.isValid());
	}
	
	@Test
	public void toPasswordNullTest() {
		Password password = passwordParse.toPassword(null);
		
		Assertions.assertNotNull(password);
	}
	
	
	@Test
	public void toPasswordDtoTest() {
		Password password = new Password();
		password.setPassword(UUID.randomUUID().toString());
		
		PasswordDto passwordDto = passwordParse.toPasswordDto(password);
		
		Assertions.assertEquals(password.getPassword(), passwordDto.getPassword());
		Assertions.assertEquals(password.isValid(), passwordDto.isValid());
	}
	
	@Test
	public void toPasswordDtoNullTest() {
		PasswordDto passwordDto = passwordParse.toPasswordDto(null);
		
		Assertions.assertNotNull(passwordDto);
	}
	
}
