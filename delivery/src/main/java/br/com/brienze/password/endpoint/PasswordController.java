package br.com.brienze.password.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.brienze.password.dto.PasswordDto;
import br.com.brienze.password.entity.Password;
import br.com.brienze.password.parse.PasswordParse;
import br.com.brienze.password.service.PasswordService;

@RestController
@RequestMapping("/v1/passwords")
public class PasswordController {

	@Autowired
	private PasswordParse passwordParse;

	@Autowired
	private PasswordService passwordService;

	@PostMapping("/validate")
	public ResponseEntity<Boolean> validate(@RequestBody PasswordDto passwordDto) {
		Password password = passwordParse.toPassword(passwordDto);
		
		PasswordDto passwordDtoResponse = passwordParse.toPasswordDto(passwordService.validate(password));

		return ResponseEntity.ok(passwordDtoResponse.isValid());
	}

}
