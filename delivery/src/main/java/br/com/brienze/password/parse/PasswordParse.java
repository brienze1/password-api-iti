package br.com.brienze.password.parse;

import org.springframework.stereotype.Component;

import br.com.brienze.password.dto.PasswordDto;
import br.com.brienze.password.entity.Password;

@Component
public class PasswordParse {

	public Password toPassword(PasswordDto passwordDto) {
		Password password = new Password();
		
		if(passwordDto != null) {
			password.setPassword(passwordDto.getPassword());
			password.setValid(passwordDto.isValid());
		}
		
		return password;
	}

	public PasswordDto toPasswordDto(Password password) {
		PasswordDto passwordDto = new PasswordDto();
		
		if(password != null) {
			passwordDto.setPassword(password.getPassword());
			passwordDto.setValid(password.isValid());
		}
		
		return passwordDto;
	}

}
