package br.com.brienze.password.rules;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class PasswordRulesTest {

	@InjectMocks
	private PasswordRules passwordRules;
	
	private String password;
	private List<String> passwordList;
	
	@BeforeEach
	public void init() {
		password = "";
		passwordList = new ArrayList<>();
	}
	
	@Test
	public void validateSuccesTest() {
		password = "AbTp9!fok";
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertTrue(isValidPassword);
	}
	
	@Test
	public void validateSuccesSpecialCaractersTest() {
		passwordList.add("AbTp9!fok");
		passwordList.add("AbTp9@fok");
		passwordList.add("AbTp9#fok");
		passwordList.add("AbTp9$fok");
		passwordList.add("AbTp9%fok");
		passwordList.add("AbTp9^fok");
		passwordList.add("AbTp9&fok");
		passwordList.add("AbTp9*fok");
		passwordList.add("AbTp9(fok");
		passwordList.add("AbTp9)fok");
		passwordList.add("AbTp9-fok");
		passwordList.add("AbTp9+fok");
		
		for (String password: passwordList) {
			Boolean isValidPassword = passwordRules.validate(password);
			
			Assertions.assertTrue(isValidPassword);
		}
	}
	
	@Test
	public void validateFailNullTest() {
		password = null;
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertFalse(isValidPassword);
	}
	
	@Test
	public void validateFailBlankTest() {
		password = " ";
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertFalse(isValidPassword);
	}
	
	@Test
	public void validateFailLessThan9CaractersTest() {
		password = "AbTp9!fo";
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertFalse(isValidPassword);
	}
	
	@Test
	public void validateFailNoDigitCaractersTest() {
		password = "AbTpi!fok";
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertFalse(isValidPassword);
	}
	
	@Test
	public void validateFailNoLowerCaseLettersTest() {
		password = "ABTP9!FOK";
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertFalse(isValidPassword);
	}
	
	@Test
	public void validateFailNoUpperCaseLettersTest() {
		password = "abtp9!fok";
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertFalse(isValidPassword);
	}
	
	@Test
	public void validateFailNoSpecialCaractersTest() {
		password = "abtp9ifok";
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertFalse(isValidPassword);
	}
	
	@Test
	public void validateFailDuplicatedCaractersTest() {
		password = "AboTp9!fok";
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertFalse(isValidPassword);
	}
	
	@Test
	public void validateFailContainsSpaceTest() {
		password = "AbTp 9!fok";
		
		Boolean isValidPassword = passwordRules.validate(password);
		
		Assertions.assertFalse(isValidPassword);
	}
	
}
