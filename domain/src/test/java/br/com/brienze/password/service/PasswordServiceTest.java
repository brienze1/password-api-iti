package br.com.brienze.password.service;

import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.brienze.password.entity.Password;
import br.com.brienze.password.rules.PasswordRules;

@ExtendWith(SpringExtension.class)
public class PasswordServiceTest {

	@InjectMocks
	private PasswordService passwordService;
	
	@Mock
	private PasswordRules passwordRules;
	
	@Test
	public void validateTest() {
		Password password = new Password();
		password.setPassword(UUID.randomUUID().toString());
		
		Mockito.when(passwordRules.validate(password.getPassword())).thenReturn(true);
		
		Password passwordResponse = passwordService.validate(password);
		
		Assertions.assertEquals(true, passwordResponse.isValid());
		Assertions.assertEquals(password.getPassword(), passwordResponse.getPassword());
	}
	
}
