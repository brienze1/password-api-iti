package br.com.brienze.password.rules;

import org.springframework.stereotype.Component;

@Component
public class PasswordRules {
	
	private static final Integer PASSWORD_MIN_LENGTH = 9;
	private static final String REGEX_DIGIT_PATTERN = "(.*[0-9].*)";
	private static final String REGEX_LOWER_CASE_PATTERN = "(.*[a-z].*)";
	private static final String REGEX_UPPER_CASE_PATTERN = "(.*[A-Z].*)";
	private static final String REGEX_SPECIAL_CHARACTERS_PATTERN = "(.*[!@#$%^&*()\\-+].*)";
	private static final String REGEX_SPACE_PATTERN = "(.*\\s+.*)";
	private static final String REGEX_DUPLICATED_CHARACTER_PATTERN = ".*(.).*\\1+\\.*.*";

	public boolean validate(String password) {
		Boolean isValidPassword = true;
		
		if(password == null || password.isBlank()) {
			return false;
		}
		
		if(password.length() < PASSWORD_MIN_LENGTH)
			isValidPassword = false;
		if(!password.matches(REGEX_DIGIT_PATTERN)) 
			isValidPassword = false;
		if(!password.matches(REGEX_LOWER_CASE_PATTERN))
			isValidPassword = false;
		if(!password.matches(REGEX_UPPER_CASE_PATTERN))
			isValidPassword = false;
		if(!password.matches(REGEX_SPECIAL_CHARACTERS_PATTERN))
			isValidPassword = false;
		if(password.matches(REGEX_DUPLICATED_CHARACTER_PATTERN))
			isValidPassword = false;
		if(password.matches(REGEX_SPACE_PATTERN))
			isValidPassword = false;
		
	   return isValidPassword;
	}

}
