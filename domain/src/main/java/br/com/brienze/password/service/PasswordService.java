package br.com.brienze.password.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.brienze.password.entity.Password;
import br.com.brienze.password.rules.PasswordRules;

@Component
public class PasswordService {
	
	@Autowired
	private PasswordRules passwordRules;

	public Password validate(Password password) {
		password.setValid(passwordRules.validate(password.getPassword()));
		
		return password;
	}

}
